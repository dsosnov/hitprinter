#pragma once //include guards analog
#include <vector>

#include "Rtypes.h"

#include "TCanvas.h"
#include "TH1F.h"
#include "TH2F.h"

#include "TTree.h"
#include "TChain.h"

#include <memory>
#include <vector>
#include <string>
#include <utility>

using std::shared_ptr;
using std::make_shared;
using std::make_unique;
using std::vector;
using std::string;
using std::pair;
using std::make_pair;

enum class workType{JINR, Gerardo, testSetup};
enum class workMode{necessary, pairs, autocorr, testMode};

namespace hitprinter{

  std::vector<std::pair<double, double>> straw_calibration_constants;

  void initStrawConstants(){
    straw_calibration_constants.resize(256, std::make_pair<double,double>(-4.074, 2.789));
  }

  double dTFine(UShort_t trigger_bcid, UShort_t trigger_tdc, UShort_t signal_bcid, UShort_t signal_tdc, UChar_t straw, bool print = false){
#ifndef DEBUG_PRINT
    if(print)
#endif
      printf("dTFine(%d, %d, %d, %d, %d) = ", trigger_bcid, trigger_tdc, signal_bcid, signal_tdc, straw);
    // if(signal_bcid < trigger_bcid)
    //   signal_bcid += 4096;
    auto cal_const = straw_calibration_constants.at(straw); // std::make_pair<double,double>(-4.074, 2.789); //
    double dt = -25*(trigger_bcid - signal_bcid) + (trigger_tdc - signal_tdc - cal_const.first)/cal_const.second;
#ifndef DEBUG_PRINT
    if(print)
#endif
      printf("%g\n", dt);
    return dt;
  }
  // double dTFine(int trigger, int signal, UShort_t* bcid, UShort_t* tdc, UChar_t straw){
  //   return dTFine(bcid[trigger], tdc[trigger], bcid[signal], tdc[signal], straw);
  // }
  
  int straw(int channel){
    // conversion of STRAW channels
    if(channel % 4 == 0) return channel / 2;
    if(channel % 4 == 3) return channel / 2;
    return -(channel / 2 + 1);
  }

  bool isTrigCh(int fec, int vmm, int ch){
    //if(fec == 2 && vmm == 9 && ch == 0)  return true; // Lucian`s Trigger
    //if(fec == 2 && vmm == 8 && ch == 63) return true; // Dubna Trigger, October runs
    //if(fec == 2 && vmm == 12 && ch == 63) return true; // Dubna Trigger, NEW Setup from 28th Oct
    //if(fec == 1 && vmm == 2 && ch == 32) return true; // mini-SRS 2Hybrids
    if(fec == 1 && vmm == 1 && ch == 63) return true; // mini-SRS 1 Hybrids

    return false;
  }


// Trigger Window Size +/- dT
  constexpr int dT0 = 50;
 
// Trigger Center
//T0 = 0; // Lucian OK
//T0 = -60; // Dubna OK
//T0 = -236; // Dubna BAD
//T0 = -2135; // Dubna BAD
  constexpr int T0 = -150; // mini SRS 1 Hybrid

  bool isTrigWindow(double TriggerTime){
		if(!((TriggerTime > T0-dT0) && (TriggerTime < T0+dT0))) return false;	// setup T0 values at top level
    return true;
  }

}


/* ************************************************************************************************************************ */
void saveHist(TH1F* hist, string name, vector<bool> log = {false, false}, string option = "h", vector<string> ext = {"root","png","pdf"}){
  hist->SetLineWidth(2);
  hist->SaveAs((name + "_src.root").c_str());
  auto c = make_shared<TCanvas>("c", "c", 1200, 1200);
  hist->Draw(option.c_str());
  if(log.size() > 0 && log.at(0))
    c->SetLogx();
  if(log.size() > 1 && log.at(1))
    c->SetLogy();
  for(auto &e: ext)
    c->SaveAs((name + "." + e).c_str());
}
void saveHist(TH2F* hist, string name, vector<bool> log = {false, false, false}, string option = "colz", vector<string> ext = {"root","png","pdf"}){
  hist->SaveAs((name + "_src.root").c_str());
  auto c = make_shared<TCanvas>("c", "c", 1200, 1200);
  c->SetLeftMargin(0.11);
  c->SetRightMargin(0.11);
  hist->Draw(option.c_str());
  if(log.size() > 0 && log.at(0))
    c->SetLogx();
  if(log.size() > 1 && log.at(1))
    c->SetLogy();
  if(log.size() > 2 && log.at(2))
    c->SetLogy();
  for(auto &e: ext)
    c->SaveAs((name + "." + e).c_str());
}

/* ************************************************************************************************************************ */
string getTimeSelection(double timeStart = 0, double timeFinish = 320){
  timeStart = (timeStart < 0) ? 0 : timeStart;
  string timeSelection = "";
  timeSelection += ((timeStart >=0) ? Form("timeS > %g", timeStart) : "");
  if(timeFinish > timeStart){
    timeSelection += (timeSelection.empty()) ? "" : " && ";
    timeSelection += Form("timeS < %g", timeFinish);
  }
  return timeSelection;
}

string lower(string s){
  string sOut = s;
  std::transform(s.begin(), s.end(), sOut.begin(),
                 [](unsigned char c){ return std::tolower(c);});
  return sOut;
}
