#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ struct Hit;
#pragma link C++ class vector<Hit>+;

#pragma link C++ struct ClusterPlane;
#pragma link C++ class vector<ClusterPlane>+;

#pragma link C++ struct ClusterDetector;
#pragma link C++ class vector<ClusterDetector>+;

#pragma link C++ struct Track;
#pragma link C++ class vector<Track>+;

#pragma link C++ class vector<int>+;
#pragma link C++ class vector<vector<int>>+;


#endif
