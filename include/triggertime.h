#pragma once //include guards analog

#include "TStyle.h"
#include "TSelector.h"
#include "TCanvas.h"
#include "TEntryList.h"

#include "TFile.h"
#include "TChain.h"

#include <memory>
#include <vector>
#include <string>
#include <utility>
#include <functional> // for old gcc's

#include "DataStructures.h"
#include "printer.h"

/* rough: the second selected as "signal", if it contents more then thr events */
vector<bool> selectTriggerTime(TTree* tree, int channel, unsigned long thr, string channelName = "channel"){
  // auto channelTree = tree->CopyTree(Form("%s == %d", channelName.c_str(), channel));  
  auto vtimes = new vector<double>();
  tree->SetBranchAddress("timeS", &vtimes);
  unsigned long maxTime = 0; //int(ceil(tree->GetMaximum("timeS")));
  for(long long i = 0; i < tree->GetEntries(); i++){
    tree->GetEntry(i);
    for(auto &t: *vtimes) if(t > maxTime) maxTime = t;
  }
  vector<unsigned long> times (maxTime+1, 0);
  // printf("maxTime: %d\n", maxTime);
  auto channels = new vector<int>();
  tree->SetBranchAddress(channelName.c_str(), &channels);

  for(long long i = 0; i < tree->GetEntries(); i++){
    tree->GetEntry(i);
    for(unsigned long t = 0; t < vtimes->size(); t++)
      if(channels->at(t) == channel){
        times.at(int(vtimes->at(t))) ++;
        // printf("%d -> %d -> %d\n", channels->at(t), int(vtimes->at(t)), times.at(int(vtimes->at(t))));
      }
  }
  // tree->ResetBranchAddresses();

  auto selectedTimes = vector<bool>(times.size(), false);
  for(auto &t: times){
    selectedTimes.push_back(t > thr);
    printf("%ld, %ld, %d\n", t, thr, int(t>thr));
  }  
  return selectedTimes;
}
vector<pair<unsigned long, unsigned long>> convertToPairs(vector<bool> selectedTimes){
  vector<pair<unsigned long, unsigned long>> selection;
  for(unsigned long i = 0, start = 0; i < selectedTimes.size(); i++){
    if(selectedTimes.at(i)){
      start = i;      
      if(selection.size() != 0 && (i == selection.back().second + 1)){
        start = selection.back().first;
        selection.pop_back();
      }
      selection.push_back(make_pair(start, i));
    }
  }
  return selection;
}

string getSelection(vector<pair<unsigned long, unsigned long>> vector, string field = "timeS"){
  string selection = "";
  for(auto &v: vector){
    selection += (selection.empty()?"":"||") +
      string(Form("(%ld <= %s && %s <= %ld)", v.first, field.c_str(), field.c_str(), v.second));
  }
  return selection;
}

TEntryList* getEntryList(TTree* tree, vector<pair<unsigned long, unsigned long>> vector, string field = "timeS"){
  auto returnList = new TEntryList();
  for(auto &v: vector){
    tree->Draw(">>elist", Form("(%ld <= %s && %s <= %ld)", v.first, field.c_str(), field.c_str(), v.second), "entrylist");  
    auto elist = (TEntryList*)(gDirectory->Get("elist"));
    returnList->Add(elist);
  }
  return returnList;
}

TTree* filterTreeByTime(TTree* treeIn, vector<bool> selection){
  static auto hitsIn = new vector<Hit>;
  treeIn->SetBranchAddress("hits", &hitsIn);
  static auto channelIn = new vector<int>();
  treeIn->SetBranchAddress("channel", &channelIn);
  static auto timeSIn = new vector<double>();
  treeIn->SetBranchAddress("timeS", &timeSIn);
  
  auto treeOut = new TTree("hits", "hits");
  static auto hitsOut = vector<Hit>();
  treeOut->Branch("hits", &hitsOut);
  static auto channelOut = vector<int>();
  treeOut->Branch("channel", &channelOut);
  static auto timeSOut = vector<double>();
  treeOut->Branch("timeS", &timeSOut);

  for(auto i = 0; i < treeIn->GetEntries(); i++){
    hitsOut.clear();
    channelOut.clear();
    timeSOut.clear();
    treeIn->GetEntry(i);
    for(unsigned long j = 0; j < hitsIn->size(); j++){
      // printf("time %d is %d\n", int(timeSIn->at(j)), int(selection.at(int(timeSIn->at(j)))));
      if(!selection.at(timeSIn->at(j)))
        continue;
      hitsOut.push_back(hitsIn->at(j));
      timeSOut.push_back(timeSIn->at(j));
      channelOut.push_back(channelIn->at(j));
    }
    if(hitsOut.size())
      treeOut->Fill();
  }
  // treeIn->ResetBranchAddresses();
  // treeOut->ResetBranchAddresses();  
  return treeOut;
}

TTree* flatten(TTree* treeIn){
  auto hitsIn = new vector<Hit>;
  treeIn->SetBranchAddress("hits", &hitsIn);
  auto channelIn = new vector<int>();
  treeIn->SetBranchAddress("channel", &channelIn);
  auto timeSIn = new vector<double>();
  treeIn->SetBranchAddress("timeS", &timeSIn);
  
  auto treeOut = new TTree("hits", "hits");
  Hit hitsOut;
  treeOut->Branch("hits", &hitsOut);
  int channelOut;
  treeOut->Branch("channel", &channelOut);
  double timeSOut;
  treeOut->Branch("timeS", &timeSOut);
  
  for(auto i = 0; i < treeIn->GetEntries(); i++){
    treeIn->GetEntry(i);
    for(unsigned long j = 0; j < hitsIn->size(); j++){
      hitsOut = hitsIn->at(j);
      timeSOut = timeSIn->at(j);
      channelOut = channelIn->at(j);
      treeOut->Fill();
    }
  }
  // treeIn->ResetBranchAddresses();
  // treeOut->ResetBranchAddresses();  
  return treeOut;
}
TTree* allToOne(TTree* treeIn){
  auto hitsIn = new vector<Hit>;
  treeIn->SetBranchAddress("hits", &hitsIn);
  auto channelIn = new vector<int>();
  treeIn->SetBranchAddress("channel", &channelIn);
  auto timeSIn = new vector<double>();
  treeIn->SetBranchAddress("timeS", &timeSIn);
  
  auto treeOut = new TTree("hits", "hits");
  vector<Hit> hitsOut;
  treeOut->Branch("hits", &hitsOut);
  vector<int> channelOut;
  treeOut->Branch("channel", &channelOut);
  vector<double> timeSOut;
  treeOut->Branch("timeS", &timeSOut);
  
  for(auto i = 0; i < treeIn->GetEntries(); i++){
    treeIn->GetEntry(i);
    hitsOut.insert(hitsOut.end(), hitsIn->begin(), hitsIn->end());
    timeSOut.insert(timeSOut.end(), timeSIn->begin(), timeSIn->end());
    channelOut.insert(channelOut.end(), channelIn->begin(), channelIn->end());
  }
  treeOut->Fill();
  // treeIn->ResetBranchAddresses();
  // treeOut->ResetBranchAddresses();  
  return treeOut;
}

