#include "DataStructures.h"

#include "TFile.h"
#include "TChain.h"

#include <memory>

// #include <memory>
#include <vector>
#include <string>
// #include <utility>
// #include <functional> // for old gcc's
// #include <tuple>
// #include <optional>

using std::make_shared;

using std::vector;
using std::string;

int main(int argc, char** argv){

  auto file = string(argv[1]);
  long long mintimeS = atoi(argv[2]);
  long long mintime = 1E9 * mintimeS;
  long long maxtimeS = atoi(argv[3]);
  long long maxtime = 1E9 * maxtimeS;
  auto fileOut = string(Form("%s_%d_%d.root", file.c_str(), mintimeS, maxtimeS));

  // printf("fileout: %s\n", fileOut.c_str());

  auto chain = make_shared<TChain>("hits"); // the same as std::make_shared<TChain>(new TChain("hits/hits"));
  chain->Add(file.c_str());
  
  auto hitsIn = new vector<Hit>;
  chain->SetBranchAddress("hits", &hitsIn);
  
  auto treeOut = new TTree("hits", "hits");
  auto hitsOut = vector<Hit>();
  treeOut->Branch("hits", &hitsOut);
  
  for(auto i = 0; i < chain->GetEntries(); i++){
    hitsOut.clear();
    chain->GetEntry(i);
    for(auto &h: (*hitsIn)){
      if(h.time < mintime || h.time > maxtime)
        continue;
      hitsOut.push_back(h);
    }
    if(hitsOut.size())
      treeOut->Fill();
  }

  treeOut->SaveAs(fileOut.c_str());

  return 0;
}
