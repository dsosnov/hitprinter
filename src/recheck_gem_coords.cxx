#include "TStyle.h"
#include "TSelector.h"
#include "TCanvas.h"
#include "TEntryList.h"

#include "TFile.h"
#include "TChain.h"

#include <getopt.h> // for getopt

#include <memory>
#include <vector>
#include <string>
#include <utility>
#include <functional> // for old gcc's
#include <tuple>
#include <optional>

#include <TH1F.h>
#include <TH2F.h>

#include "DataStructures.h"
#include "printer.h"
#include "triggertime.h"

using std::shared_ptr;
using std::make_shared;
using std::make_unique;
using std::vector;
using std::string;
using std::pair;
using std::make_pair;
using std::tuple;
using std::make_tuple;
using std::min;
using std::max;
using std::get;

using globalCoord = std::tuple<unsigned int, unsigned int, int>;

int chinv(int ch){
  return 64*4 - ch;
}

globalCoord gerCoord12(Hit hit){
  globalCoord coord = make_tuple(0,0,0);
  if(hit.fec == 2 && hit.vmm >= 8 && hit.vmm <= 11){ // Straws and scintillators
    if(hit.vmm == 10 || hit.vmm == 11) // Straws
      return {4, 0, hit.ch + (hit.vmm - 10) * 64}; // Why 0???
    else // Scintillators
      return {99, 0, hit.ch + (hit.vmm - 8) * 64};
  } else {
    switch(hit.vmm){
    case 0:
    case 1:
      coord = {1 + (hit.fec-1)*2, 1, 64*2 + hit.ch + hit.vmm * 64};
      coord = (hit.fec == 2) ? coord : make_tuple(get<0>(coord), get<1>(coord), chinv(get<2>(coord)));
      return coord;
    case 2:
    case 3:
      coord = {1 + (hit.fec-1)*2, 1, hit.ch + (hit.vmm-2) * 64};
      coord = (hit.fec == 2) ? coord : make_tuple(get<0>(coord), get<1>(coord), chinv(get<2>(coord)));
      return coord;
    case 4:
    case 5:
      coord = {1 + (hit.fec-1)*2, 0, 64*2 + hit.ch + (hit.vmm-4) * 64};
      coord = (hit.fec == 2) ? coord : make_tuple(get<0>(coord), get<1>(coord), chinv(get<2>(coord)));
      return coord;
    case 6:
    case 7:
      coord = {1 + (hit.fec-1)*2, 0, hit.ch + (hit.vmm-6) * 64};
      coord = (hit.fec == 2) ? coord : make_tuple(get<0>(coord), get<1>(coord), chinv(get<2>(coord)));
      return coord;

    case 8:
    case 9:
      coord = {2 + (hit.fec-1)*2, 0, 64*2 + hit.ch + (hit.vmm-8) * 64};
      coord = (hit.fec == 2) ? coord : make_tuple(get<0>(coord), get<1>(coord), chinv(get<2>(coord)));
      return coord;
    case 10:
    case 11:
      coord = {2 + (hit.fec-1)*2, 0, hit.ch + (hit.vmm-10) * 64};
      coord = (hit.fec == 2) ? coord : make_tuple(get<0>(coord), get<1>(coord), chinv(get<2>(coord)));
      return coord;
    case 12:
    case 13:
      coord = {2 + (hit.fec-1)*2, 1, 64*2 + hit.ch + (hit.vmm-12) * 64};
      return coord;
    case 14:
    case 15:
      coord = {2 + (hit.fec-1)*2, 1, hit.ch + (hit.vmm-14) * 64};
      return coord;
    }
  }
  return coord;
}

// globalCoord getCoord(Hit hit, int setup){
//   switch(setup){
//   case(0):
    
//   }
// }

int main(int argc, char** argv){

  auto file = string(argv[1]);
  // auto fileOut = string(Form("%s_%d_%d.root", file.c_str(), mintimeS, maxtimeS));

  // printf("fileout: %s\n", fileOut.c_str());

  auto chain = make_shared<TChain>("hits"); // the same as std::make_shared<TChain>(new TChain("hits/hits"));
  chain->Add(file.c_str());
  
  auto hitsIn = new vector<Hit>;
  chain->SetBranchAddress("hits", &hitsIn);
    
  for(auto i = 0; i < chain->GetEntries(); i++){
    chain->GetEntry(i);
    for(auto &h: (*hitsIn)){
      auto coord = gerCoord12(h);
      if(h.det != get<0>(coord) ||  h.plane != get<1>(coord) || h.pos != get<2>(coord)){
        printf("Coords for (%d, %d, %d) from converter: %d, %d, %d; Coords from hardware: %d, %d, %d\n", h.fec, h.vmm, h.ch, h.det, h.plane, h.pos, get<0>(coord), get<1>(coord), get<2>(coord));
        exit(1);
      }
    }
  }
  printf("Coordinates was the same!\n");

  return 0;
}
