#include "TStyle.h"
#include "TSelector.h"
#include "TCanvas.h"
#include "TEntryList.h"

#include "TFile.h"
#include "TChain.h"

#include <getopt.h> // for getopt

#include <memory>
#include <vector>
#include <string>
#include <utility>
#include <functional> // for old gcc's
#include <tuple>
#include <optional>

#include <TH1F.h>
#include <TH2F.h>

#include "DataStructures.h"
#include "printer.h"
#include "triggertime.h"

// #define DEBUG_PRINT

using std::shared_ptr;
using std::make_shared;
using std::make_unique;
using std::vector;
using std::string;
using std::pair;
using std::make_pair;
using std::tuple;
using std::make_tuple;
using std::min;
using std::max;

using HitTimeChannelTuple = tuple<Hit, int, double>;

/* ************************************************************************************************************************ */
void printSize(TTree* tree, string selection = ""){
  printf("printSize...\n");
  tree->Draw("@hits.size() >> hsize", selection.c_str(), "");
  auto hsize = shared_ptr<TH1F>(static_cast<TH1F*>(gDirectory->Get("hsize")));
  hsize->SetTitle("Size of Hit vectors;@hits.size; N");
  saveHist(hsize.get(), "size");
}

void printChannels(TTree* tree, string selection = "", int nChannels = 1024){
  printf("printChannels...\n");
  selection += (selection.empty()) ? "" : "&&";
  selection += Form("channel > 0 && channel < %d", nChannels);
  tree->Draw("channel >> hchannel", selection.c_str(), "");
  auto hchannel = shared_ptr<TH1F>(static_cast<TH1F*>(gDirectory->Get("hchannel")));
  hchannel->SetTitle("Number of events per channel (beam profile) ;channel; N");
  saveHist(hchannel.get(), "hits_channel");
}

void printADC(TTree* tree, string selection = ""){
  printf("printADC...\n");
  tree->Draw("hits.adc >> hadc", selection.c_str(), "");
  auto hadc = shared_ptr<TH1F>(static_cast<TH1F*>(gDirectory->Get("hadc")));
  hadc->SetTitle("ADC for all channels ;ADC; N");
  saveHist(hadc.get(), "adc_spectre");
}

void printADCStraws(TTree* tree, string selection = "", int nChannels = 64){
  printf("printADCStraws...\n");
  selection += (selection.empty()) ? "" : "&&";
  selection += Form("channel > 0 && channel < %d", nChannels);
  tree->Draw("hits.adc >> hadc", selection.c_str(), "");
  auto hadc = shared_ptr<TH1F>(static_cast<TH1F*>(gDirectory->Get("hadc")));
  hadc->SetTitle("ADC for straws only;ADC; N");
  saveHist(hadc.get(), "adc_spectre_straws");
}

void printADCvsChannel(TTree* tree, string selection = ""){
  printf("printADCvsChannel...\n");
  tree->Draw("hits.adc:channel >> hadcch", selection.c_str(), "colz");
  auto hadsch = shared_ptr<TH2F>(static_cast<TH2F*>(gDirectory->Get("hadcch")));
  hadsch->SetTitle("ADC per channel ;channel; ADC");
  saveHist(hadsch.get(), "adc_channel");
}

void printSizevsTime(TTree* tree, string selection = ""){
  printf("printSizevsTime...\n");
  tree->Draw("@hits.size():hits.time/1E9 >> hst", selection.c_str(), "colz");
  auto hsize = shared_ptr<TH2F>(static_cast<TH2F*>(gDirectory->Get("hst")));
  hsize->SetTitle("Hits vector size per time ;time, [s]; @hits.size");
  saveHist(hsize.get(), "size_time");
}

void printADCvsTime(TTree* tree, string selection = ""){
  printf("printADCvsTime...\n");
  tree->Draw("hits.adc:hits.time/1E9/60 >> hADCt", selection.c_str(), "colz");
  auto hADCt = shared_ptr<TH2F>(static_cast<TH2F*>(gDirectory->Get("hADCt")));
  hADCt->SetTitle("ADC per time ;time, [s];ADC");
  saveHist(hADCt.get(), "hADCt");
}

void printChannelsvsTime(TTree* tree, string selection = "", double timeStart = 0, double timeFinish = 320){
  printf("printChannelsvsTime...\n");
  tree->Draw(Form("channel:timeS >> htSc(%d, %d, %d, 192, -64, 128)", int(timeFinish - timeStart), int(timeStart), int(timeFinish)), selection.c_str(), "colz");
  // tree->Draw("channel:timeS/60 >> htSc", "channel >= 0", "colz");
  auto htSc = shared_ptr<TH2F>(static_cast<TH2F*>(gDirectory->Get("htSc")));
  htSc->SetTitle("Number of events in channel per time ;time, [s];channel");
  saveHist(htSc.get(), "channels_time");
}
void printChannelsvsTimeStrawOnly(TTree* tree, string selection = "", double timeStart = 0, double timeFinish = 320){
  printf("printChannelsvsTimeStrawOnly...\n");
  tree->Draw(Form("channel:timeS >> htSc0(%d, %d, %d, 64, 0, 64)", int(timeFinish - timeStart), int(timeStart), int(timeFinish)), selection.c_str(), "colz");
  auto htSc0 = shared_ptr<TH2F>(static_cast<TH2F*>(gDirectory->Get("htSc0")));
  htSc0->SetTitle("Number of events in channel per time for straws only;time, [s];channel");
  saveHist(htSc0.get(), "channels_time_straw");
}

void printTimeForChannel(TTree* tree, int channel, string selection = ""){
  printf("printTimeForChannel...\n");
  tree->Draw("timeS >> htch80(320, 0, 320)", Form("channel == %d", channel), "");
  auto htch80 = shared_ptr<TH1F>(static_cast<TH1F*>(gDirectory->Get("htch80")));
  htch80->SetTitle(Form("Number of events time in channel %d;time, [s];N", channel));
  saveHist(htch80.get(), Form("time_channel_%d", channel), {false, true}, "h");
}

/* ************************************************************************************************************************ */
void printTwoCh(TTree* tree, int channel1 = 13, int channel2 = 14, double dtMax = 100){
  printf("printTwoCh...\n");
  auto histTimeShift = make_shared<TH1F>(Form("deltaTime_%d_%d", channel1, channel2), Form("deltaTime_%d_%d", channel1, channel2), 1000, -500, 500);

  static auto hits = new vector<Hit>;
  tree->SetBranchAddress("hits", &hits);
  static auto channel = new vector<int>();
  tree->SetBranchAddress("channel", &channel);
  static auto timeS = new vector<double>();
  tree->SetBranchAddress("timeS", &timeS);
  for(auto i = 0; i < tree->GetEntries(); i++){
    tree->GetEntry(i);
    for(unsigned long k1 = 0; k1 < hits->size(); k1++){
      if(channel->at(k1) != channel1) continue;
      for(unsigned long k2 = k1+1; k2 < hits->size(); k2++){
        if(channel->at(k2) != channel2) continue;
        double dtMax = hitprinter::dTFine(0, 0, hits->at(k2).bcid, hits->at(k2).tdc, channel->at(k2)) -
          hitprinter::dTFine(0, 0, hits->at(k1).bcid, hits->at(k1).tdc, channel->at(k1));
        histTimeShift->Fill(dtMax);
      }
    }
  }
  
  saveHist(histTimeShift.get(), Form("timeshift_%d_%d", channel1, channel2), {false, false});
}
vector<tuple<HitTimeChannelTuple, HitTimeChannelTuple, double>> getHitPairs(TTree* tree, unsigned int nChannels = 64, double dtMax = 1E6){
  vector<tuple<HitTimeChannelTuple, HitTimeChannelTuple, double>> pairs;
  
  static auto hits = new vector<Hit>;
  tree->SetBranchAddress("hits", &hits);
  static auto channel = new vector<int>();
  tree->SetBranchAddress("channel", &channel);
  static auto timeS = new vector<double>();
  tree->SetBranchAddress("timeS", &timeS);

  for(auto i = 0; i < tree->GetEntries(); i++){
    tree->GetEntry(i);
    for(unsigned long k1 = 0; k1 < hits->size(); k1++){
      if(channel->at(k1) < 0 || channel->at(k1) >= nChannels) continue;
      for(unsigned long k2 = k1+1; k2 < hits->size(); k2++){
        if(channel->at(k2) < 0 || channel->at(k2) >= nChannels) continue;
        if(channel->at(k2) == channel->at(k1)) continue;
        if(fabs(channel->at(k2) - channel->at(k1)) != 1) continue;
        auto kMin = (channel->at(k1) < channel->at(k2)) ? k1 : k2;
        auto kMax = (channel->at(k1) < channel->at(k2)) ? k2 : k1;
        double dt = hits->at(k2).time - hits->at(k1).time;
        if(fabs(dt) > 1E5){ // dTFine will not work when difference > 1E5
          if(dt > 0)
            break;
          else
            continue;
        }
        // hitprinter::dTFine(0, 0, hits->at(k2).bcid, hits->at(k2).tdc, channel->at(k2)) -
        // hitprinter::dTFine(0, 0, hits->at(k1).bcid, hits->at(k1).tdc, channel->at(k1));
        // printf("Times for hits in channels %d and %d: %g (%g) and %g (%g)\n", channel->at(kMin), channel->at(kMax),
        //        hits->at(kMin).time, hitprinter::dTFine(0, 0, hits->at(kMin).bcid, hits->at(kMin).tdc, channel->at(kMin)),
        //        hits->at(kMax).time, hitprinter::dTFine(0, 0, hits->at(kMax).bcid, hits->at(kMax).tdc, channel->at(kMax)));
        dt = hitprinter::dTFine(0, 0, hits->at(k2).bcid, hits->at(k2).tdc, channel->at(k2)) -
          hitprinter::dTFine(0, 0, hits->at(k1).bcid, hits->at(k1).tdc, channel->at(k1));
        if(dt + dtMax < 0) continue;
        else if(dt -dtMax > 0) break;
        // dt = (kMin == k1) ? dt : -dt;
        auto tupleMin = make_tuple(hits->at(kMin), channel->at(kMin), timeS->at(kMin));
        auto tupleMax = make_tuple(hits->at(kMax), channel->at(kMax), timeS->at(kMax));
        pairs.push_back(make_tuple(tupleMin, tupleMax, dt));
      }
    }
  }
  std::sort(pairs.begin(), pairs.end(), [](auto a, auto b){ return std::get<0>(std::get<0>(a)).time < std::get<0>(std::get<0>(b)).time;});
  
  return pairs;
}
vector<HitTimeChannelTuple> getHitsInChannel(TTree* tree, int triggerChannel){
  vector<HitTimeChannelTuple> triggers;
  static auto hits = new vector<Hit>;
  tree->SetBranchAddress("hits", &hits);
  static auto channel = new vector<int>();
  tree->SetBranchAddress("channel", &channel);
  static auto timeS = new vector<double>();
  tree->SetBranchAddress("timeS", &timeS);

  for(auto i = 0; i < tree->GetEntries(); i++){
    tree->GetEntry(i);
    for(unsigned long j = 0; j < hits->size(); j++){
      if(channel->at(j) != triggerChannel) continue;
      triggers.push_back(make_tuple(hits->at(j), channel->at(j), timeS->at(j)));
    }
  }

  std::sort(triggers.begin(), triggers.end(), [](auto a, auto b){ return std::get<0>(a).time < std::get<0>(b).time;});

  return triggers;
}
vector<HitTimeChannelTuple> getAllHits(TTree* tree){
  vector<HitTimeChannelTuple> triggers;
  static auto hits = new vector<Hit>;
  tree->SetBranchAddress("hits", &hits);
  static auto channel = new vector<int>();
  tree->SetBranchAddress("channel", &channel);
  static auto timeS = new vector<double>();
  tree->SetBranchAddress("timeS", &timeS);

  for(auto i = 0; i < tree->GetEntries(); i++){
    tree->GetEntry(i);
    for(unsigned long j = 0; j < hits->size(); j++){
      triggers.push_back(make_tuple(hits->at(j), channel->at(j), timeS->at(j)));
    }
  }

  std::sort(triggers.begin(), triggers.end(), [](auto a, auto b){ return std::get<0>(a).time < std::get<0>(b).time;});

  return triggers;
}
void printPairPlots(TTree* tree, int triggerChannel = 80, unsigned int nChannels = 64, int selectedChannel = 13,
                    double timeStart = 0, double timeFinish = 320,
                    double dtMax = 100, double dtWindow = 250){
  printf("printPairPlots...\n");

  auto hist = make_shared<TH2F>("pairs", "pairs; time, [s]; channel", int(timeFinish - timeStart), timeStart, timeFinish, nChannels, 0, nChannels);

  // auto histTimeDifferenceWOcut = make_shared<TH1F>("histTimeDifferenceWOcut; [ns]", Form("(t2 - t1) for straws %d and %d w/o dt cut; ns; N", selectedChannel, selectedChannel+1), 1000, -500, 500);
  // auto histTimeDifferenceWOcutAll = make_shared<TH1F>("histTimeDifferenceWOcutAll; [ns]", Form("(t2 - t1) for all straws w/o dt cut; ns; N"), 1000, -500, 500);
  auto histTimeDifference = make_shared<TH1F>("histTimeDifference; [ns]", Form("(t2 - t1) for straws %d and %d w/ dt cut; ns; N", selectedChannel, selectedChannel+1), 1000, -500, 500);
  auto histTimeDifferenceAll = make_shared<TH1F>("histTimeDifferenceAll; [ns]", Form("histTimeDifference for all straws w/ dt cut; ns; N"), 1000, -500, 500);
  
  auto histTimeShift = make_shared<TH1F>("histTimeShift; [ns]", Form("t0 from straw %d to trigger %d w/ dt cut; ns; N", selectedChannel, triggerChannel), 2000, -1000, 1000);
  auto histTimeShiftAll = make_shared<TH1F>("histTimeShiftAll; [ns]", Form("t0 for all straws to trigger %d w/ dt cut; ns; N", triggerChannel), 2000, -1000, 1000);
  auto histTimeShift2 = make_shared<TH1F>("histTimeShift2; [ns]", Form("t0 from straw %d when previous was triggered also to trigger %d w/ dt cut; ns; N", selectedChannel+1, triggerChannel), 1000, -1000, 0);
  auto histTimeShiftAll2 = make_shared<TH1F>("histTimeShiftAll2; [ns]", Form("t0 for all straws when previous was triggered also to trigger %d w/ dt cut; ns; N", triggerChannel), 1000, -1000, 0);
  
  auto pairs = getHitPairs(tree, nChannels, dtMax);

  auto histTimeCheck = make_shared<TH1F>("timeCheck", Form("timeCheck; time [ns]; (dTFine - time) / time"), 1E6, 0, 1E6);
  auto triggers = getHitsInChannel(tree, triggerChannel);
  for(auto &t: triggers)
    histTimeCheck->Fill(std::get<0>(t).time, (std::get<0>(t).time - hitprinter::dTFine(0, 0, std::get<0>(t).bcid, std::get<0>(t).tdc, std::get<1>(t))) - std::get<0>(t).time);
  saveHist(histTimeCheck.get(), "histTimeCheck");

  /* removing all hit pairs with large time difference */
  pairs.erase(std::remove_if(pairs.begin(), pairs.end(),
                             [dtMax](auto p){return fabs(std::get<2>(p)) > dtMax;}),
              pairs.end());
  printf("Number of pairs at all:      %ld; number of trigger hits: %ld\n", pairs.size(), triggers.size());


  /* creating vector of hit pairs for channel */
  // vector<tuple<HitTimeChannelTuple, HitTimeChannelTuple, double>> pairsch;
  // std::copy_if(pairs.begin(), pairs.end(),
  //              std::back_inserter(pairsch),
  //              [selectedChannel](auto p){return std::get<1>(std::get<0>(p)) == selectedChannel;});
  // printf("Number of pairs for channel: %ld\n", pairsch.size());

  unsigned long tMin = 0;
  for(auto &p: pairs){
    histTimeDifferenceAll->Fill(std::get<2>(p));
    hist->Fill(std::get<2>(std::get<0>(p)), std::get<1>(std::get<0>(p)));
    auto p1 = std::get<0>(std::get<0>(p));
    auto p2 = std::get<0>(std::get<1>(p));
    auto ch1 = std::get<1>(std::get<0>(p));
    if(ch1 == selectedChannel)
      histTimeDifference->Fill(std::get<2>(p));
    for(unsigned long t = tMin; t < triggers.size(); t++){
      auto tHit = std::get<0>(triggers.at(t));
      auto dtns = tHit.time - p1.time;
      if(fabs(dtns) > 1E5){ // dTFine will not work when difference > 1E5
        if(dtns > 0)
          break;
        else{
          tMin++;
          continue;
        }
      }
      auto dtf1 = hitprinter::dTFine(tHit.bcid, tHit.tdc, p1.bcid, p1.tdc, std::get<1>(std::get<0>(p)));
      if(dtns - dtWindow > 0){ // Trigger before signal
        tMin++;
        continue; 
      } else if(dtf1 + dtWindow < 0) // Trigger after signal
        break; 
      auto dtf2 = hitprinter::dTFine(tHit.bcid, tHit.tdc, p2.bcid, p2.tdc, std::get<1>(std::get<1>(p)));
      histTimeShiftAll->Fill(dtf1);
      histTimeShiftAll2->Fill(dtf2);
      if(ch1 == selectedChannel){
        histTimeShift->Fill(dtf1);
        histTimeShift2->Fill(dtf2);
      }
    }
  }

  saveHist(hist.get(), "pairs_per_time", {false, false, false});
  saveHist((TH1F*)(hist->ProjectionX()), "pairs_per_time_proj_time", {false, false});
  saveHist((TH1F*)(hist->ProjectionY()), "pairs_per_time_proj_channel", {false, false});
  saveHist(histTimeDifference.get(), Form("pairs_timesdiff_%d", selectedChannel), {false, false});
  saveHist(histTimeDifferenceAll.get(), "pairs_timesdiff_all", {false, false});
  // saveHist(histTimeDifferenceWOcut.get(), Form("pairs_timesdiff_wocut_%d", selectedChannel), {false, false});
  // saveHist(histTimeDifferenceWOcutAll.get(), "pairs_timesdiff_wocut_all", {false, false});
  saveHist(histTimeShift.get(), Form("pairs_timeshift_%d_%d", selectedChannel,triggerChannel), {false, false});
  saveHist(histTimeShiftAll.get(), Form("pairs_timeshift_all_%d",triggerChannel), {false, false});
  saveHist(histTimeShift2.get(), Form("pairs_timeshift2_%d_%d", selectedChannel,triggerChannel), {false, false});
  saveHist(histTimeShiftAll2.get(), Form("pairs_timeshift2_all_%d",triggerChannel), {false, false});
}
void printNHitsPerTrigger(TTree* tree, int triggerChannel = 80, unsigned int nChannels = 64, double dtWindow = 250){
  printf("printNHitsPerTrigger...\n");
  auto hits = getAllHits(tree);
  
  auto nHits = make_shared<TH1F>("nHits", Form("Number of hits in window around trigger %d", triggerChannel), 10, 0, 10);

  double dt;
  for(unsigned long k = 0; k < hits.size(); k++){
    if(std::get<1>(hits.at(k)) == triggerChannel){
      int c = 0;
      if(k > 0)
        for(unsigned long i = k-1; i > 0; i--){
          if(std::get<1>(hits.at(i)) == triggerChannel) continue;
          dt = hitprinter::dTFine(std::get<0>(hits.at(k)).bcid, std::get<0>(hits.at(k)).tdc,
                                  std::get<0>(hits.at(i)).bcid, std::get<0>(hits.at(i)).tdc, std::get<1>(hits.at(i)));
          if(fabs(dt) > dtWindow) break;
          c++;
        }
      for(unsigned long i = k+1; i < hits.size(); i++){
        if(std::get<1>(hits.at(i)) == triggerChannel) continue;
        dt = hitprinter::dTFine(std::get<0>(hits.at(k)).bcid, std::get<0>(hits.at(k)).tdc,
                                std::get<0>(hits.at(i)).bcid, std::get<0>(hits.at(i)).tdc, std::get<1>(hits.at(i)));
        if(fabs(dt) > dtWindow) break;
        c++;
      }
      nHits->Fill(c);
    }
  }

  saveHist(nHits.get(), Form("nHits_%d", triggerChannel), {false, true});  
}
void printAutoCorrPlots(TTree* tree, int triggerChannel = 80, unsigned int nChannels = 64, int selectedChannel = 13,
                        double timeShift = -12, double dtMax = 100, double dtWindow = 250){
  printf("printAutoCorrPlots...\n");

  auto autocorrAll = make_shared<TH2F>("autocorrAll", Form("Autocorellation from any channel pair to trigger %d", triggerChannel), 100, 0, 100, 100, 0, 100); 
  auto autocorr = make_shared<TH2F>("autocorr", Form("Autocorellation for channels %d and %d to trigger %d", selectedChannel, selectedChannel+1, triggerChannel), 100, 0, 100, 100, 0, 100); 

  auto timeShiftT1All = make_shared<TH1F>("acTimeShiftT1All", Form("Time from trigger %d to lesser channel in pair for any channel pair", triggerChannel), 1000, -500, 500); 
  auto timeShiftT2All = make_shared<TH1F>("acTimeShiftT2All", Form("Time from trigger %d to greater channel in pair for any channel pair", triggerChannel), 1000, -500, 500); 
  auto timeShiftT1 = make_shared<TH1F>("acTimeShiftT1", Form("Time from trigger %d to channel %d in pair with %d", triggerChannel, selectedChannel, selectedChannel+1), 1000, -500, 500); 
  auto timeShiftT2 = make_shared<TH1F>("acTimeShiftT2", Form("Time from trigger %d to channel %d in pair with %d", triggerChannel, selectedChannel+1, selectedChannel), 1000, -500, 500); 
  auto timeShiftT1wocutAll = make_shared<TH1F>("acTimeShiftT1wocutAll", Form("Time from trigger %d to lesser channel in pair for any channel pair (w/o cut by time to trigger)", triggerChannel), 1000, -500, 500); 
  auto timeShiftT2wocutAll = make_shared<TH1F>("acTimeShiftT2wocutAll", Form("Time from trigger %d to greater channel in pair for any channel pair (w/o cut by time to trigger)", triggerChannel), 1000, -500, 500); 
  auto timeShiftT1wocut = make_shared<TH1F>("acTimeShiftT1wocut", Form("Time from trigger %d to channel %d in pair with %d (w/o cut by time to trigger)", triggerChannel, selectedChannel, selectedChannel+1), 1000, -500, 500); 
  auto timeShiftT2wocut = make_shared<TH1F>("acTimeShiftT2wocut", Form("Time from trigger %d to channel %d in pair with %d (w/o cut by time to trigger)", triggerChannel, selectedChannel+1, selectedChannel), 1000, -500, 500); 

  auto pairs = getHitPairs(tree, nChannels);
  auto triggers = getHitsInChannel(tree, triggerChannel);

  /* removing all hit pairs with large time difference */
  pairs.erase(std::remove_if(pairs.begin(), pairs.end(),
                             [dtMax](auto p){return fabs(std::get<2>(p)) > dtMax;}),
              pairs.end());

  auto pMin = 0;
  for(auto &t: triggers){
    auto tHit = std::get<0>(t);
    for(unsigned long pN = pMin; pN < pairs.size(); pN++){
      auto pair = pairs.at(pN);
      auto p1 = std::get<0>(std::get<0>(pair));
      auto dtns = tHit.time - p1.time;
      if(fabs(dtns) > 1E5){ // dTFine will not work when difference > 1E5
        if(dtns > 0){ // pair before trigger
          pMin++;
          continue;
        } else  // pair too far away after trigger
          break;
      }
      
      auto p1ch = std::get<1>(std::get<0>(pair));
      auto p2 = std::get<0>(std::get<1>(pair));
      auto dtf1 = hitprinter::dTFine(tHit.bcid, tHit.tdc, p1.bcid, p1.tdc, std::get<1>(std::get<0>(pair))) - timeShift;
      auto dtf2 = hitprinter::dTFine(tHit.bcid, tHit.tdc, p2.bcid, p2.tdc, std::get<1>(std::get<1>(pair))) - timeShift;
      timeShiftT1wocutAll->Fill(dtf1);
      timeShiftT2wocutAll->Fill(dtf2);
      if(p1ch == selectedChannel){
        timeShiftT1wocut->Fill(dtf1);
        timeShiftT2wocut->Fill(dtf2);
      }
      /*If trigger before signal => dT > 0; Otherwise, dT < 0*/
      
      if(dtf1 > dtWindow){
        pMin++;
        continue;
      } else if(dtf1 < 0)
        break;
      timeShiftT1All->Fill(dtf1);
      timeShiftT2All->Fill(dtf2);
      autocorrAll->Fill(dtf1, dtf2);
      if(p1ch == selectedChannel){
        timeShiftT1->Fill(dtf1);
        timeShiftT2->Fill(dtf2);
        autocorr->Fill(dtf1, dtf2);
      }
    }
  }

  saveHist(timeShiftT1All.get(), Form("autocorr_timeShiftT1All_%d", triggerChannel));
  saveHist(timeShiftT2All.get(), Form("autocorr_timeShiftT2All_%d", triggerChannel));
  saveHist(timeShiftT1.get(), Form("autocorr_timeShiftT1_%d_%d", selectedChannel, triggerChannel));
  saveHist(timeShiftT2.get(), Form("autocorr_timeShiftT2_%d_%d", selectedChannel, triggerChannel));

  saveHist(timeShiftT1wocutAll.get(), Form("autocorr_timeShiftT1wocutAll_%d", triggerChannel));
  saveHist(timeShiftT2wocutAll.get(), Form("autocorr_timeShiftT2wocutAll_%d", triggerChannel));
  saveHist(timeShiftT1wocut.get(), Form("autocorr_timeShiftT1wocut_%d_%d", selectedChannel, triggerChannel));
  saveHist(timeShiftT2wocut.get(), Form("autocorr_timeShiftT2wocut_%d_%d", selectedChannel, triggerChannel));

  saveHist(autocorr.get(), Form("autocorr_%d_%d", selectedChannel, triggerChannel));
  saveHist(autocorrAll.get(), Form("autocorrAll_%d", triggerChannel));
}

/* ************************************************************************************************************************ */
TTree* convertMu2E(string filename){
  // auto fileIn = TFile::Open(filename.c_str(), "read");
  // auto chain = static_cast<TTree*>(fileIn->Get("vmm"));
  
  auto chain = make_shared<TChain>("vmm"); // the same as std::make_shared<TChain>(new TChain("hits/hits"));
  chain->Add(filename.c_str());

  // /* eventSize : vector<int> */
  // auto eventSizeIn = new vector<int>;
  // chain->SetBranchAddress("eventSize", &eventSizeIn);
  // /* relbcid   : vector<vector<int> > */
  // auto relbcidIn = new vector<vector<int>>;
  // chain->SetBranchAddress("relbcid", &relbcidIn);
  /* triggerCounter : vector<int> */
  static auto triggerCounterIn = new vector<int>;
  chain->SetBranchAddress("triggerCounter", &triggerCounterIn);
  /* boardId   : vector<int> */
  static auto boardIdIn = new vector<int>;
  chain->SetBranchAddress("boardId", &boardIdIn);
  /* chip      : vector<int> */
  static auto chipIn = new vector<int>;
  chain->SetBranchAddress("chip", &chipIn);
  /* daq_timestamp_s : vector<int> */
  static auto daq_timestamp_sIn = new vector<int>;
  chain->SetBranchAddress("daq_timestamp_s", &daq_timestamp_sIn);
  /* daq_timestamp_ns : vector<int> */
  static auto daq_timestamp_nsIn = new vector<int>;
  chain->SetBranchAddress("daq_timestamp_ns", &daq_timestamp_nsIn);
  /* channel   : vector<vector<int> > */
  static auto channelIn = new vector<vector<int>>;
  chain->SetBranchAddress("channel", &channelIn);
  /* bcid      : vector<vector<int> > */
  static auto bcidIn = new vector<vector<int>>;
  chain->SetBranchAddress("bcid", &bcidIn);
  /* tdo       : vector<vector<int> > */
  static auto tdoIn = new vector<vector<int>>;
  chain->SetBranchAddress("tdo", &tdoIn);
  /* pdo       : vector<vector<int> > */
  static auto pdoIn = new vector<vector<int>>;
  chain->SetBranchAddress("pdo", &pdoIn);
    
  auto treeOut = new TTree("hits", "hits");
  static auto hitsOut = vector<Hit>();
  treeOut->Branch("hits", &hitsOut);
  Hit hit;

  if(!chain->GetEntries()){
    printf("No events in file! Exiting...\n");
    exit(0);
  }

  std::optional<int> daq_timestamp_sIn_min = std::nullopt;
  for(auto i = 0; i < chain->GetEntries(); i++){
    chain->GetEntry(i);
    for(unsigned long j = 0; j < triggerCounterIn->size(); j++){
      if(!daq_timestamp_sIn_min || daq_timestamp_sIn_min.value() > daq_timestamp_sIn->at(j))
        daq_timestamp_sIn_min = daq_timestamp_sIn->at(j);
    }
  }
  printf("Minumal time: %d\n", daq_timestamp_sIn_min.value());
  for(auto i = 0; i < chain->GetEntries(); i++){
    hitsOut.clear();
    chain->GetEntry(i);
    for(unsigned long j = 0; j < triggerCounterIn->size(); j++){
      hit.det = 0;
      hit.plane = 0;
      hit.readout_time = 0;
      hit.pos = 0;
      hit.over_threshold = true;
      hit.id = triggerCounterIn->at(j);
      hit.fec = boardIdIn->at(j);
      hit.vmm = chipIn->at(j);
      hit.time = (daq_timestamp_sIn->at(j) - daq_timestamp_sIn_min.value()) * 1E9 + daq_timestamp_nsIn->at(j);
      hit.chip_time = daq_timestamp_sIn->at(j);
      for(unsigned long k = 0; k < channelIn->at(j).size(); k++){
        hit.ch = channelIn->at(j).at(k);
        hit.bcid = bcidIn->at(j).at(k);
        hit.tdc = tdoIn->at(j).at(k);
        hit.adc = pdoIn->at(j).at(k);
        hitsOut.push_back(hit);
        // hitsOut.push_back(static_cast<Hit>({0}));
      }
    }
    treeOut->Fill();
  }

  // chain->ResetBranchAddresses();  
  // treeOut->ResetBranchAddresses();
  
  // treeOut->Print();
  // treeOut->SaveAs(Form("%s-converted.root", filename.c_str()));

  return treeOut;
}
/* ************************************************************************************************************************ */
int updateTree(shared_ptr<TTree> treeIn, shared_ptr<TTree> treeOut, std::function<int(Hit)> channelConverter){
  auto nEntries = treeIn->GetEntries();
  printf("tree Entries: %lld\n", nEntries);

  // auto vhits = make_shared<HitVector>();
  // treeIn->SetBranchAddress("hits", &(*vhits));
  auto hits = new vector<Hit>;
  treeIn->SetBranchAddress("hits", &hits);
  // auto hitsData = make_shared<Hits2>(treeIn.get());

  auto channel = vector<int>();
  treeOut->Branch("channel", &channel);

  auto timeS = vector<double>();
  treeOut->Branch("timeS", &timeS);

  double maxTime = 0, currentTime;

  for(auto i = 0; i < nEntries; i++){
    treeIn->GetEntry(i);
    channel.clear();
    timeS.clear();
    // printf("n hits: %ld\n", hits->size());
    for(auto &hit: *hits){
      // channel.push_back(hitprinter::straw(hit.ch + hit.vmm*64));
      channel.push_back(channelConverter(hit));
      currentTime = double(hit.time) / 1E9;
      timeS.push_back(currentTime);
      if(currentTime > maxTime)
        maxTime = currentTime;
    }
    treeOut->Fill();
  }
  
  treeIn->ResetBranchAddresses();
  treeOut->ResetBranchAddresses();

  return maxTime;
}

std::map<string,std::function<int(Hit)>> channelMap = {
  {"jinr", [](Hit hit) // before 12.11.2021
    {
      return hitprinter::straw(hit.ch + hit.vmm*64);
    }
  },
  {"jinr14", [](Hit hit) // before start from 14.11.2021
    {
      if(hit.vmm == 0 && hit.ch == 63)
        return 64;
      return hit.ch + hit.vmm*64;
    }
  },
  {"gerardo", [](Hit hit) // before 12.11.2021
    {
      // straws: detector 4, 
      if(hit.vmm == 12 || hit.vmm == 13){
        if(hit.fec == 1){ // Straws
          // printf("straw %d %d %d %d -> %d\n", hit.det, hit.fec, hit.vmm, hit.ch, 64 + (hit.vmm-12));
          return hitprinter::straw(hit.ch + 64 * (hit.vmm-12));
        } else if(hit.fec == 2){ // Scintillators
          // printf("scint %d %d %d %d -> %d\n", hit.det, hit.fec, hit.vmm, hit.ch, 64 + (hit.vmm-12));
          return 64 + (hit.vmm-12);
        }
      } else {// Started from the end of negative straws
        return -1 * (64 + hit.ch + 64 * hit.vmm + hit.fec * 64 * 12);
      }
      return -1 * (64 + 2 * 64 * 12 + 1); 
    }
  },
  {"jinr12", [](Hit hit) // since 12.11.2021 - scintillators on 5'th channel of fec1
    {
      auto vmm = hit.vmm;
      if(vmm == 10 || vmm == 11)
        vmm -= 8;
      return hitprinter::straw(hit.ch + vmm*64);
    }
  },
  {"gerardo12", [](Hit hit) // since 12.11.2021
    {
      // straws: detector 4,
      if(hit.fec == 2){
        if(hit.vmm == 10 || hit.vmm == 11){ // Straws
          return hitprinter::straw(hit.ch + 64 * (hit.vmm-10));
        } else if(hit.vmm == 8 || hit.vmm == 9){ // Scintillators
          return 64 + (hit.vmm-8);
        }
      } else {// Started from the end of negative straws
        return -1 * (64 + hit.ch + 64 * hit.vmm + hit.fec * 64 * 12);
      }
      return -1 * (64 + 2 * 64 * 12 + 1); 
    }
  }
};

std::function<int(Hit)> getChannelFunction(std::map<string,std::function<int(Hit)>> channelMap, workType type, unsigned int date){
  if (type == workType::testSetup){
    return channelMap.at("jinr");
  }else if (type == workType::Gerardo){
    if(date < 12)
      return channelMap.at("gerardo");
    else
      return channelMap.at("gerardo12");
  }else if (type == workType::JINR){
    if(date < 12)
      return channelMap.at("jinr");
    else if(date < 14)
      return channelMap.at("jinr12");
    else
      return channelMap.at("jinr14");
  }
  return channelMap.at("jinr");
}

/* ************************************************************************************************************************ */
TTree* filter(TTree* tree, std::function<bool(Hit)> filter = [](Hit){return true;}){
  static auto hitsIn = new vector<Hit>;
  tree->SetBranchAddress("hits", &hitsIn);
  static auto channelIn = new vector<int>();
  tree->SetBranchAddress("channel", &channelIn);
  static auto timeSIn = new vector<double>();
  tree->SetBranchAddress("timeS", &timeSIn);
  
  auto treeOut = new TTree("hitsFiltered", "hitsFiltered");
  static auto hitsOut = vector<Hit>();
  treeOut->Branch("hits", &hitsOut);
  static auto channelOut = vector<int>();
  treeOut->Branch("channel", &channelOut);
  static auto timeSOut = vector<double>();
  treeOut->Branch("timeS", &timeSOut);

  for(auto i = 0; i < tree->GetEntries(); i++){
    hitsOut.clear();
    channelOut.clear();
    timeSOut.clear();
    tree->GetEntry(i);
    for(unsigned long j = 0; j < hitsIn->size(); j++){
      if(!filter(hitsIn->at(j)))
        continue;
      hitsOut.push_back(hitsIn->at(j));
      timeSOut.push_back(timeSIn->at(j));
      channelOut.push_back(channelIn->at(j));
    }
    if(hitsOut.size())
      treeOut->Fill();
  }
  // treeIn->ResetBranchAddresses();
  // treeOut->ResetBranchAddresses();  
  return treeOut;
}
int main(int argc, char** argv){
  string file = "";

  // TODO: std::variant from <variant>

  string setup = "test";
  string useMode = "necessary"; // necessary (n), pair, autocorr or test
  double timeShift = 0;
  int triggerChannel = 80;
  int pickedChannel = 15;
  double timeStart = 0;
  double timeFinish = 0;
  double dtMax = 300;
  int date = 0;
  double dtWindow = 250;

  bool mu2e = 0;

  char c;
  while (1)
  {
    static struct option long_options[] =
      {
        /* These options don’t set a flag.
           We distinguish them by their indices. */
        {"file",      required_argument,     0, 'f'},
        {"setup",     required_argument,     0, 's'}, // jinr, gerardo, test, 
        {"useMode",   required_argument,     0, 'm'},
        {"timeshift", required_argument,     0, 'a'},
        {"trigger",   required_argument,     0, 'b'},
        {"channel",   required_argument,     0, 'c'},
        {"date",      required_argument,     0, 'd'},
        {"start",     required_argument,     0, 'l'},
        {"finish",    required_argument,     0, 'm'},
        {"dtmax",     required_argument,     0, 'g'},
        {"dtwindow",  required_argument,     0, 'i'},
        {"help",      required_argument,     0, 'h'},
        {"mu2e",      no_argument,           0, 'u'},
        // {"hybrid",    no_argument,       &mu2e,  0},
        {0, 0, 0, 0}
      };
    /* getopt_long stores the option index here. */
    int option_index = 0;

    c = getopt_long (argc, argv, "f:s:m:a:b:c:d:l:m:g:",
                     long_options, &option_index);
    /* Detect the end of the options. */
    if (c == -1)
      break;

    switch (c)
    {
      case 'f':
        file = string(optarg);
        break;
      case 's':
        setup = lower(string(optarg));
        break;
      case 'm':
        useMode = lower(string(optarg));
        break;
      case 'a':
        timeShift = std::atof(optarg);
        break;
      case 'b':
        triggerChannel = std::atoi(optarg);
        break;
      case 'c':
        pickedChannel = std::atoi(optarg);
        break;
      case 'd':
        date = std::atoi(optarg);
        break;
      case 'k':
        timeStart = std::atof(optarg);
        break;
      case 'l':
        timeFinish = std::atof(optarg);
        break;
      case 'g':
        dtMax = std::atof(optarg);
        break;
      case 'i':
        dtWindow = std::atof(optarg);
        break;
      case 'u':
        mu2e = true;
        break;
      case 'h':
        printf("Possible options:\n");
        printf("\t--file or -f\n");
        printf("\t--setup or -s\n");
        printf("\t--useMode or -m\n");
        printf("\t--timeshift\n");
        printf("\t--trigger\n");
        printf("\t--channel\n");
        printf("\t--date or -d\n");
        printf("\t--start\n");
        printf("\t--finish\n");
        printf("\t--dtmax\n");
        printf("\t--dtwindow\n");
        printf("\t--help or -h\n");
        break;
      case '?':
        /* getopt_long already printed an error message. */
        break;

      default:
        abort ();
    }
  }
  if (optind < argc) {
    while (optind < argc)
      file = string(argv[optind++]);
  }
  if(file.empty())
    file = "dataset.root";

  printf("Processing file  %s...\n", file.c_str());
  printf("Setup:           %s\n", setup.c_str());
  printf("Date for setup:  %d\n", date);
  printf("Mode:            %s\n", useMode.c_str());

  /* **************************************************************************************************** */
  gStyle->SetLineScalePS(1);
  gStyle->SetOptStat(0);

  auto typeOfWork = workType::testSetup;
  if(setup == "jinr"){
    typeOfWork = workType::JINR;
  } else if(setup == "g" || setup == "gerardo"){
    typeOfWork = workType::Gerardo;
  } else if(setup == "t" || setup == "test" || setup == "tests"){
    typeOfWork = workType::testSetup;
  }
  auto mode = workMode::testMode;
  if(useMode == "necessary" || useMode == "n"){
    mode = workMode::necessary;
  } else if(useMode == "pair" || useMode == "pairs" || useMode == "p"){
    mode = workMode::pairs;
  } else if(useMode == "autocorr" || useMode == "a"){
    mode = workMode::autocorr;
  }
  
  hitprinter::initStrawConstants();

  // if(1990 < hit.bcid && hit.bcid < 25900) return false;
  std::function<bool(Hit)> filterFunctionDefault = [timeStart, timeFinish](Hit hit){
    // if(hit.time/1E9 < timeStart || hit.time/1E9 > timeFinish) return false; // TODO
    return (hit.bcid > 0 || hit.tdc > 0);
  };
  std::function<bool(Hit)> filterFunctionJINR = [filterFunctionDefault](Hit hit){
    if(!filterFunctionDefault(hit)) return false;
    if(hit.vmm == 2) return false;
    else if(hit.vmm == 3) return (hit.ch == 48);
    return true;
  };
  std::function<bool(Hit)> filterFunctionJINR12 = [filterFunctionDefault](Hit hit){
    if(!filterFunctionDefault(hit)) return false;
    if(hit.vmm == 10) return false;
    else if(hit.vmm == 11) return (hit.ch == 48);
    return true;
  };
  std::function<bool(Hit)> filterFunctionJINR14 = [filterFunctionDefault](Hit hit){
    if(!filterFunctionDefault(hit)) return false;
    return true;
  };
  auto filterFunction = filterFunctionDefault;
  if(typeOfWork == workType::JINR){
    if(date < 12)
      filterFunction = filterFunctionJINR;
    else if (date < 14)
      filterFunction = filterFunctionJINR12;
    else
      filterFunction = filterFunctionJINR14;
  }

  auto channelFunction = getChannelFunction(channelMap, typeOfWork, date);

  int nChannels; //, trig, pickedChannel, timeshift = 0;
  switch(typeOfWork){
    case workType::JINR:
      //     timeshift = -500;
      //     trig = 120;
      nChannels = (date < 14) ? 64: 63;
      //     pickedChannel = 15; //58
      //     timeshift = 0;
      break;
    case workType::Gerardo:
      //     trig = 64;
      nChannels = 63;
      //     pickedChannel = 13;
      //     timeshift = 0;
      break;
    case workType::testSetup:
      //     trig = 80;
      nChannels = 64;
      //     pickedChannel = 15;
      //     timeshift = -40;
      break;
  }


  /* **************************************************************************************************** */  
  shared_ptr<TTree> treeIn;
  if(mu2e){
    printf("Mu2E mode enabled...\n");
    treeIn = shared_ptr<TTree>(convertMu2E(file));
  } else {
    auto chain = make_shared<TChain>("hits"); // the same as std::make_shared<TChain>(new TChain("hits/hits"));
    chain->Add(file.c_str());
    treeIn = static_cast<shared_ptr<TTree>>(chain);  
  }
  
  auto anTree0 = make_shared<TTree>("hitsUpd", "");
  anTree0->AddFriend(treeIn.get());

  auto t = updateTree(treeIn, anTree0, channelFunction);
  if(timeFinish < 0 || timeFinish <= timeStart) timeFinish = t + 1;
             
  bool dofilter = true;
  auto anTree = (!dofilter) ? anTree0 : shared_ptr<TTree>(filter(anTree0.get(), filterFunction));

  /* **************************************************************************************************** */

  auto timeSelection = getTimeSelection(timeStart, timeFinish);

  gStyle->SetOptStat(1);
  if(mode == workMode::necessary){
    gStyle->SetOptStat(0);
    printChannelsvsTime(anTree.get(), timeSelection, timeStart, timeFinish);
    printChannelsvsTimeStrawOnly(anTree.get(), timeSelection, timeStart, timeFinish);
    printChannels(anTree.get(), timeSelection, 128);
    printADC(anTree.get(), timeSelection);
    printADCStraws(anTree.get(), timeSelection, nChannels);
    printADCvsChannel(anTree.get(), timeSelection);
    printSize(anTree.get(), timeSelection);
  } else if(mode == workMode::pairs){
    if(typeOfWork == workType::Gerardo){
      printTwoCh(anTree.get(), 64, 65);
    }
    printTimeForChannel(anTree.get(), triggerChannel, timeSelection);
    printNHitsPerTrigger(anTree.get(), triggerChannel, nChannels, dtWindow);
    printPairPlots(anTree.get(), triggerChannel, nChannels, pickedChannel, timeStart, timeFinish, dtMax, 1e4); // Window is 1E6 ns
  } else if(mode == workMode::autocorr){
    printAutoCorrPlots(anTree.get(), triggerChannel, nChannels, pickedChannel, timeShift, dtMax, dtWindow); // timeshift = 12 for zeroing halfsum
  } else if(mode == workMode::testMode){
    
  }
    
  // // printTimeForChannel(anTree1);
  // // printADCChannel80(anTree1);
  // printTimeForChannel(anTree.get());
  // printADCChannel80(anTree.get());

  
  /* **************************************************************************************************** */


  
  return 0;
}
